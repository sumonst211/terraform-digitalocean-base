#!/usr/bin/env bash

THEHOME="$(su dbuser-backup -c '$HOME')"

sudo docker exec mysql /usr/bin/mysqldump -u dbuser_sa --password=newpassword5 ejs > /home/dbuser-backup/terraform-digitalocean-base/db-backup/ejs-db-backup-`date +'%Y%m%d%H%M%S'`.sql && sleep 1 &
BACK_PID=$!
wait $BACK_PID
sudo chown -R dbuser-backup:dbuser-backup /home/dbuser-backup/terraform-digitalocean-base/db-backup
echo "done"

BASE="/home/dbuser-backup/terraform-digitalocean-base/"
cd $BASE


sudo -i -u dbuser-backup bash << EOF
    BASE="/home/dbuser-backup/terraform-digitalocean-base/"
    cd $BASE && git add db-backup; git commit -m 'generated backup'; git push -o ci.skip; git push
EOF
